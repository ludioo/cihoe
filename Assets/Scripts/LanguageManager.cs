﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LanguageManager : MonoBehaviour
{
    GameObject[] indo;
    GameObject[] eng;
    public bool indoLanguage;
    public bool engLanguage;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        indo = GameObject.FindGameObjectsWithTag("Indo");
        eng = GameObject.FindGameObjectsWithTag("Eng");

        if (indoLanguage == false && engLanguage == true)
        {
            foreach(GameObject i in indo)
            {
                Destroy(i.gameObject);
                Debug.Log("Destroy Indo)");
            }
        }
        
        if(engLanguage == false && indoLanguage == true)
        {
            foreach(GameObject e in eng)
            {
                Destroy(e.gameObject);
                Debug.Log("Destroy English");
            }
        }
    }

    public void PilihIndo()
    {
        indoLanguage = true;
        engLanguage = false;
    }

    public void PilihEnglish()
    {
        engLanguage = true;
        indoLanguage = false;
    }
}
