﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Level1Manager : MonoBehaviour
{
    public Wire[] allWires;
    public int totalScore;
    public int currentScore;
    public int totalWiredDone;
    public GameObject[] winPanel;
    public GameObject[] losePanel;
    LanguageManager languageManager;
    Database database;
    public Text currentScoreText;
    public Text textWinIndo;
    public Text textLoseIndo;
    public Text textWinEng;
    public Text textLoseEng;
    public Animator[] animators;

    // Start is called before the first frame update
    void Start()
    {
        database = GameObject.FindGameObjectWithTag("DB").GetComponent<Database>();
        languageManager = GameObject.FindGameObjectWithTag("LM").GetComponent<LanguageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        currentScoreText.text = allWires.Sum(x => x.score).ToString();
        textWinIndo.text = allWires.Sum(x => x.score).ToString();
        textLoseIndo.text = allWires.Sum(x => x.score).ToString();
        textWinEng.text = allWires.Sum(x => x.score).ToString();
        textLoseEng.text = allWires.Sum(x => x.score).ToString();

        if (IsCompletedWired())
        {
            if(IsCompletedScore())
            {
                if (languageManager.indoLanguage == true)
                {
                    Debug.Log("MenangIndo");
                    winPanel[0].SetActive(true);
                    losePanel[0].SetActive(false);

                }
                else
                {
                    Debug.Log("MenangEnglish");
                    winPanel[1].SetActive(true);
                    losePanel[1].SetActive(false);
                }
            }
            else
            {
                if (languageManager.indoLanguage == true)
                {
                    Debug.Log("KalahIndo");
                    winPanel[0].SetActive(false);
                    losePanel[0].SetActive(true);

                }
                else
                {
                    Debug.Log("KalahEnglish");
                    winPanel[1].SetActive(false);
                    losePanel[1].SetActive(true);
                }
            }
            
        }
      
        Debug.Log("Score : " + allWires.Sum(x => x.score));
        Debug.Log("Wired : " + allWires.Sum(x => x.wireDone));
    }

    public bool IsCompletedScore()
    {
        return allWires.Sum(x => x.score) >= 70;
    }

    public bool IsCompletedWired()
    {
        return allWires.Sum(x => x.wireDone) == 10; 
    }
}
