﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Wire : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private Image image;
    public Color customColor;
    private bool isDragStarted = false;
    private LineRenderer lineRenderer;
    private Canvas canvas;
    private Vector3 position;
    public Vector2 movePos;
    public Vector2 movePosMinimY;
    public Vector2 movePosMaxY;
    public bool canDrag;
    public int wireDone;
    public int score;
    Level1Manager level1Manager;
    LanguageManager languageManager;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        lineRenderer = GetComponent<LineRenderer>();
        canvas = GetComponentInParent<Canvas>();
        wireDone = 0;
        score = 0;
        level1Manager = GameObject.FindGameObjectWithTag("L1M").GetComponent<Level1Manager>();
        languageManager = GameObject.FindGameObjectWithTag("LM").GetComponent<LanguageManager>();


    }

    // Update is called once per frame
    void Update()
    {
        if (isDragStarted)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out movePos);
            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, canvas.transform.TransformPoint(movePos));
        }
        else if(canDrag == true)
        {
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.zero);
        }

        if(wireDone == 10)
        {
            if(score >= 70)
            {
                Debug.Log("Menang");
            }
            else
            {
                Debug.Log("Salah");
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(canDrag)
        {
            isDragStarted = true;
            position = transform.position;
        }

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = position;
        isDragStarted = false;
        Debug.Log(movePos);
        if(movePos.y >= movePosMinimY.y && movePos.y <= movePosMaxY.y || movePos.y <= movePosMinimY.y && movePos.y >= movePosMaxY.y)
        {
            if(languageManager.indoLanguage == true)
            {
                level1Manager.animators[0].Play("Benar");
                canDrag = false;
                wireDone++;
                score += 10;
            }
            else
            {
                level1Manager.animators[1].Play("Benar");
                canDrag = false;
                wireDone++;
                score += 10;
            }
        }
        else
        {
            if(languageManager.indoLanguage == true)
            {
                level1Manager.animators[2].Play("Benar");
                score -= 10;
                canDrag = true;
            }
            else
            {
                level1Manager.animators[3].Play("Benar");
                score -= 10;
                canDrag = true;
            }
            
        }
        
    }

}
