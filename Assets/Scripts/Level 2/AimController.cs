﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    Vector2 mousePosition;
    public Camera mainCamera;
    private Rigidbody2D rb;
    Cannon cannon;
    // Start is called before the first frame update
    void Start()
    {
        rb = GameObject.FindGameObjectWithTag("Cannon").GetComponent<Rigidbody2D>();
        cannon = GameObject.FindGameObjectWithTag("Cannon").GetComponent<Cannon>();
    }

    // Update is called once per frame
    void Update()
    {
        Aim();
    }

    private void FixedUpdate()
    {
        Move();
    }

    public void Aim()
    {
        if(Input.GetMouseButtonDown(0))
        {
            cannon.Shoot();
        }
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

    public void Move()
    {
        Vector2 aimDirection = mousePosition - rb.position;
        float aimAngle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = aimAngle;
    }

}
