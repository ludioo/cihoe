﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    Rigidbody2D rb;
    public SpriteRenderer spriteRenderer;
    Cannon cannon;
    // Start is called before the first frame update
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        cannon = GameObject.FindGameObjectWithTag("Cannon").GetComponent<Cannon>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.gameObject.tag == this.gameObject.tag)
        {
            Destroy(this.gameObject);
            cannon.score += 10;
            cannon.phase++;
        }
        else if(collision.gameObject.tag != this.gameObject.tag && collision.gameObject.tag != "dinding")
        {
            Destroy(this.gameObject);
            cannon.score -= 10;
            cannon.phase++;
        }
        else if(collision.gameObject.tag == "dinding")
        {
            Destroy(this.gameObject);
        }
    }
}
