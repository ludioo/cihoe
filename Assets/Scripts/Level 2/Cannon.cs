﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cannon : MonoBehaviour
{
    public GameObject[] bullets;
    public Transform point;
    public int[] index;
    public float shootForce;
    public SpriteRenderer spriteNow;
    public SpriteRenderer spriteNext;
    public Ball[] ball;
    public int score;
    public Text textScore;
    public Text panelIndo;
    public Text panelEng;
    public int phase;
    public GameObject[] panelWin;
    LanguageManager languageManager;
    public SpriteRenderer[] spriteCannon;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        index[0] = Random.Range(0, 10);
        RandomIndex();
        languageManager = GameObject.FindGameObjectWithTag("LM").GetComponent<LanguageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        spriteNow.sprite = ball[index[1]].spriteRenderer.sprite;
        spriteNext.sprite = ball[index[0]].spriteRenderer.sprite;
        Debug.Log("score :" + score);
        textScore.text = score.ToString();
        if(phase >= 10)
        {
            spriteCannon[0].sortingOrder = -1;
            spriteCannon[1].sortingOrder = -1;
            spriteCannon[2].sortingOrder = -1;

            if (languageManager.indoLanguage == true)
            {
                panelWin[0].SetActive(true);
                panelIndo.text = score.ToString();
            }
            else
            {
                panelWin[1].SetActive(true);
                panelEng.text = score.ToString();
            }


        }
    }

    public void Shoot()
    {
        GameObject bullet = Instantiate(bullets[index[1]], point.position, point.rotation);
        bullet.GetComponent<Rigidbody2D>().AddForce(point.up * shootForce, ForceMode2D.Impulse);
        RandomIndex();
    }
    
    public void RandomIndex()
    {
        index[1] = Random.Range(0, 10);
        index[1] = index[0];
        index[0] = Random.Range(0, 10);
        
        spriteNow.sprite = ball[index[1]].spriteRenderer.sprite;
    }
}
