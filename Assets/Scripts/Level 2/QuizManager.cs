﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour
{
    public GameObject[] soalIndo;
    public GameObject[] PembahasanIndo;
    public GameObject[] soalEnglish;
    public GameObject[] pembahasanEnglish;
    public GameObject[] panelSoal;
    public GameObject[] panelPembahasan;
    public GameObject[] panelWin;
    public GameObject[] panelLose;
    public Text scoreText;
    public Text scorePanelWinIndo;
    public Text scorePanelWinEng;
    public Text scorePanelLoseIndo;
    public Text scorePanelLoseEng;
    public int index;
    public int score;
    LanguageManager languageManager;
    public Animator[] animators;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        languageManager = GameObject.FindGameObjectWithTag("LM").GetComponent<LanguageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(index > 9)
        {
            if(score >= 70)
            {
                if (languageManager.indoLanguage == true)
                {
                    panelSoal[0].SetActive(false);
                    panelWin[0].SetActive(true);
                }
                else
                {
                    panelSoal[1].SetActive(false);
                    panelWin[1].SetActive(true);
                }

                Debug.Log("PanelMenang");
            }
            else
            {
                if (languageManager.indoLanguage == true)
                {
                    panelSoal[0].SetActive(false);
                    panelLose[0].SetActive(true);
                }
                else
                {
                    panelSoal[1].SetActive(false);
                    panelLose[1].SetActive(true);
                }

                Debug.Log("PanelKalah");
            }
            
        }

        if(languageManager.indoLanguage == true)
        {
            scorePanelWinIndo.text = score.ToString();
            scorePanelLoseIndo.text = score.ToString();
        }
        else
        {
            scorePanelWinEng.text = score.ToString();
            scorePanelLoseEng.text = score.ToString();
        }
        scoreText.text = score.ToString();

    }

    public void CheckJawaban(bool jawaban)
    {
        if(languageManager.indoLanguage == true)
        {
            if (jawaban == true)
            {
                score += 10;
                panelSoal[0].SetActive(false);
                animators[0].Play("Benar");
                panelPembahasan[0].SetActive(true);
                soalIndo[index].SetActive(false);
                PembahasanIndo[index].SetActive(true);
                PembahasanIndo[index - 1].SetActive(false);
                
            }
            else if (jawaban == false)
            {
                score -= 10;
                soalIndo[index].SetActive(false);
                if (index >= 9)
                {
                    animators[2].Play("Benar");
                    index++;
                }
                else
                {
                    animators[2].Play("Benar");
                    soalIndo[index + 1].SetActive(true);
                    index++;
                }

            }
        }
        else
        {
            if (jawaban == true)
            {
                score += 10;
                panelSoal[1].SetActive(false);
                animators[1].Play("Benar");
                panelPembahasan[1].SetActive(true);
                soalEnglish[index].SetActive(false);
                pembahasanEnglish[index].SetActive(true);
                pembahasanEnglish[index - 1].SetActive(false);
            }
            else if (jawaban == false)
            {
                score -= 10;
                soalEnglish[index].SetActive(false);
                if (index >= 9)
                {
                    animators[3].Play("Benar");
                    index++;
                }
                else
                {
                    animators[3].Play("Benar");
                    soalEnglish[index + 1].SetActive(true);
                    index++;
                }

            }
        }
        
        
        Debug.Log("Score : " + score);
        Debug.Log("Index : " + index);

    }

    public void LanjutSoal()
    {
        if(languageManager.indoLanguage == true)
        {
            PembahasanIndo[index].SetActive(false);
            panelPembahasan[0].SetActive(false);
            soalIndo[index].SetActive(false);
            panelSoal[0].SetActive(true);
            if (index == 9)
            {
                index++;
            }
            else
            {
                soalIndo[index + 1].SetActive(true);
                index++;
            }
        }
        else
        {
            pembahasanEnglish[index].SetActive(false);
            panelPembahasan[1].SetActive(false);
            soalEnglish[index].SetActive(false);
            panelSoal[1].SetActive(true);
            if (index == 9)
            {
                index++;
            }
            else
            {
                soalEnglish[index + 1].SetActive(true);
                index++;
            }
        }


        Debug.Log("Index : " + index);
    }
}
