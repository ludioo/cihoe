﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragnDrop : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler,IPointerDownHandler
{
    [SerializeField] private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;
    public  Vector3 resetPosition;
    public ItemSlot[] itemSlot;

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        canvasGroup.alpha = .6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
        if(itemSlot[0].dropped == true)
        {
            return;
            
        }

        if (itemSlot[1].dropped == true)
        {

            return;
        }

        if (itemSlot[0].dropped == false)
        {
            this.transform.localPosition = new Vector3(resetPosition.x, resetPosition.y, resetPosition.z);

        }

        if (itemSlot[1].dropped == false)
        {
            this.transform.localPosition = new Vector3(resetPosition.x, resetPosition.y, resetPosition.z);

        }


    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(itemSlot[0].dropped == true)
        {
            itemSlot[0].dropped = false;
        }

        if (itemSlot[1].dropped == true)
        {
            itemSlot[1].dropped = false;
        }
    }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        resetPosition = this.transform.localPosition;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetPosition()
    {
        this.transform.localPosition = new Vector3(resetPosition.x, resetPosition.y, resetPosition.z);
    }
}
