﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemSlot : MonoBehaviour,IDropHandler
{
    public bool dropped = false;
    public string jawaban;
    public GameObject itemJawaban;
    public DragnDrop dragNDrop;


    public void OnDrop(PointerEventData eventData)
    {
        
        dropped = true;
        Debug.Log("OnDrop");
        if(eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        jawaban = collision.gameObject.name;
        itemJawaban = collision.gameObject;
        dragNDrop = collision.gameObject.GetComponent<DragnDrop>();
        Debug.Log(jawaban);
    }

    // Start is called before the first frame update
    void Start()
    {

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Reset()
    {
        
    }
}
