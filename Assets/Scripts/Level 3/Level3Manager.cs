﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level3Manager : MonoBehaviour
{
    public ItemSlot[] itemSlot;
    public int phase;
    public int score;
    private DragnDrop dragNDrop;
    public GameObject[] winPanel;
    public GameObject[] losePanel;
    public GameObject[] soalIndo;
    public GameObject[] soalEnglish;
    LanguageManager languageManager;
    public Text scoreText;
    public Text scoreWinIndo;
    public Text scoreWinEng;
    public Text scoreLoseIndo;
    public Text scoreLoseEng;
    public Animator[] animators;
    // Start is called before the first frame update
    void Start()
    {
        phase = 1;
        score = 0;
        dragNDrop = GameObject.FindGameObjectWithTag("DNG").GetComponent<DragnDrop>();
        languageManager = GameObject.FindGameObjectWithTag("LM").GetComponent<LanguageManager>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = score.ToString();
        scoreWinIndo.text = score.ToString();
        scoreWinEng.text = score.ToString();
        scoreLoseIndo.text = score.ToString();
        scoreLoseEng.text = score.ToString();

        if (phase > 6)
        {
            if(score >= 70)
            {
                if (languageManager.indoLanguage == true)
                {
                    Debug.Log("MenangIndo");
                    winPanel[0].SetActive(true);
                    losePanel[0].SetActive(false);

                }
                else
                {
                    Debug.Log("MenangEnglish");
                    winPanel[1].SetActive(true);
                    losePanel[1].SetActive(false);
                }
            }
            else
            {
                if (languageManager.indoLanguage == true)
                {
                    Debug.Log("KalahIndo");
                    winPanel[0].SetActive(false);
                    losePanel[0].SetActive(true);

                }
                else
                {
                    Debug.Log("KalahEnglish");
                    winPanel[1].SetActive(false);
                    losePanel[1].SetActive(true);
                }
            }
        }
    }

    public void CheckJawaban()
    {
        switch (phase)
        {
            case 1:
                if (itemSlot[0].jawaban == "Na" && itemSlot[1].jawaban == "K" || itemSlot[0].jawaban == "K" && itemSlot[1].jawaban == "Na")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if(languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                    
                    
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                    
                }
                break;

            case 2:
                if(itemSlot[0].jawaban == "F" || itemSlot[1].jawaban == "F")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score += 10;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score += 10;
                        Debug.Log(score);
                    }
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score -= 10;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score -= 10;
                        Debug.Log(score);
                    }
                }
                break;

            case 3:
                if (itemSlot[0].jawaban == "He" && itemSlot[1].jawaban == "Ne" || itemSlot[0].jawaban == "Ne" && itemSlot[1].jawaban == "He")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                }
                break;

            case 4:
                if (itemSlot[0].jawaban == "O" && itemSlot[1].jawaban == "S" || itemSlot[0].jawaban == "S" && itemSlot[1].jawaban == "O")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                }
                break;

            case 5:
                if (itemSlot[0].jawaban == "Ca" && itemSlot[1].jawaban == "Mg" || itemSlot[0].jawaban == "Mg" && itemSlot[1].jawaban == "Ca")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score += 20;
                        Debug.Log(score);
                    }
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        phase++;
                        soalIndo[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        phase++;
                        soalEnglish[phase - 1].SetActive(true);
                        score -= 20;
                        Debug.Log(score);
                    }
                }
                break;

            case 6:
                if (itemSlot[0].jawaban == "H" || itemSlot[1].jawaban == "H")
                {
                    Debug.Log("Benar");
                    Destroy(itemSlot[0].itemJawaban);
                    Destroy(itemSlot[1].itemJawaban);
                    Debug.Log("reset");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[0].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        score += 10;
                        phase++;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[1].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        score += 10;
                        phase++;
                        Debug.Log(score);
                    }
                }
                else
                {
                    itemSlot[0].dragNDrop.ResetPosition();
                    itemSlot[1].dragNDrop.ResetPosition();
                    Debug.Log("Salah");
                    if (languageManager.indoLanguage == true)
                    {
                        animators[2].Play("Benar");
                        soalIndo[phase - 1].SetActive(false);
                        score -= 10;
                        phase++;
                        Debug.Log(score);
                    }
                    else
                    {
                        animators[3].Play("Benar");
                        soalEnglish[phase - 1].SetActive(false);
                        score -= 10;
                        phase++;
                        Debug.Log(score);
                    }
                }
                break;
        }
    }
}
