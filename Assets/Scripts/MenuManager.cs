﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    Database database;
    // Start is called before the first frame update
    void Start()
    {
        database = GameObject.FindGameObjectWithTag("DB").GetComponent<Database>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void JumpToScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void OpenLevel(int sceneIndex)
    {
        database.indexLevel++;
        SceneManager.LoadScene(sceneIndex); 
    }

    public void Exit()
    {
        Application.Quit();
    }
}
