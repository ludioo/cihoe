﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetunjukManager : MonoBehaviour
{
    public GameObject levelIndo;
    public GameObject[] textLevelIndo;

    public GameObject levelEng;
    public GameObject[] textLevelEng;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowLevel1Indo()
    {
        levelIndo.SetActive(false);
        textLevelIndo[0].SetActive(true);
    }

    public void CloseLevel1Indo()
    {
        levelIndo.SetActive(true);
        textLevelIndo[0].SetActive(false);
    }

    public void ShowLevel2Indo()
    {
        levelIndo.SetActive(false);
        textLevelIndo[1].SetActive(true);
    }
    public void CloseLevel2Indo()
    {
        levelIndo.SetActive(true);
        textLevelIndo[1].SetActive(false);
    }

    public void ShowLevel3Indo()
    {
        levelIndo.SetActive(false);
        textLevelIndo[2].SetActive(true);
    }
    public void CloseLevel3Indo()
    {
        levelIndo.SetActive(true);
        textLevelIndo[2].SetActive(false);
    }

    public void ShowLevel1Eng()
    {
        levelEng.SetActive(false);
        textLevelEng[0].SetActive(true);
    }
    public void CloseLevel1Eng()
    {
        levelEng.SetActive(true);
        textLevelEng[0].SetActive(false);
    }
    public void ShowLevel2Eng()
    {
        levelEng.SetActive(false);
        textLevelEng[1].SetActive(true);
    }
    public void CloseLevel2Eng()
    {
        levelEng.SetActive(true);
        textLevelEng[1].SetActive(false);
    }
    public void ShowLevel3Eng()
    {
        levelEng.SetActive(false);
        textLevelEng[2].SetActive(true);
    }
    public void CloseLevel3Eng()
    {
        levelEng.SetActive(true);
        textLevelEng[2].SetActive(false);
    }
}
