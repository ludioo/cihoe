﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PilihLevelManager : MonoBehaviour
{
    public Button[] levelButton;
    Database database;
    // Start is called before the first frame update
    void Start()
    {
        database = GameObject.FindGameObjectWithTag("DB").GetComponent<Database>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(database.indexLevel)
        {
            case 2:
                levelButton[1].interactable = true;
                levelButton[2].interactable = false;
                break;
            case 3:
                levelButton[1].interactable = true;
                levelButton[2].interactable = true;
                break;

        }
    }

}
